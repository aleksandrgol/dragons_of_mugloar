# Dragons of Mugloar

## Description:
This project is test job for BigBank front-end position.

***Requirements:***
* Create a web app (using the api documented here) which:
* Allows the user to start a game
* Fetches and displays the list of ads
* Allows user to pick which ones to solve
* Allows user to purchase items from the shop
* Displays player's score, gold and lives

***References:***

[Dragons of Mugloar page](https://dragonsofmugloar.com/)

[Dragons of Mugloar API](https://dragonsofmugloar.com/doc/)

***Developer comments:***

In some places I used `setTimeout` to display some UI loading process.
This code can be refactored and improved as well, adding better error handlers and API health checks.

You can test this app in mobile phone:
* connect your mobile phone to same wi-fi as your machine
* run in terminal `yarn serve`
* then go to url printed in terminal on your mobile phone

```
yarn serve
```


## Development:
```
yarn
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
