import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const gameDataKeys = [
  "gameId",
  "lives",
  "gold",
  "level",
  "score",
  "highScore",
  "turn"
];

const reputationInitData = {
  people: 0,
  state: 0,
  underworld: 0
};

let stateInitData = {
  ads: JSON.parse(localStorage.getItem("ads")) || null,
  reputation:
    JSON.parse(localStorage.getItem("reputation")) || reputationInitData,
  shop: JSON.parse(localStorage.getItem("shop")) || null
};

gameDataKeys.forEach(
  key => (stateInitData[key] = localStorage.getItem(key) || null)
);

const getNumber = value => {
  if (typeof value !== "number") {
    return parseInt(value, 10);
  }
  return value;
};

export default new Vuex.Store({
  state: stateInitData,
  mutations: {
    clearState: state => {
      Object.keys(state).forEach(key => {
        state[key] = null;
        localStorage.removeItem(key);
      });
      state.reputation = reputationInitData;
    },
    setData: (state, { key, value }) => {
      state[key] = value;
      if (typeof value === ("object" || "array")) {
        localStorage.setItem(key, JSON.stringify(value));
      } else {
        localStorage.setItem(key, value);
      }
    },
    setReputation: (state, data) => {
      state.reputation = data;
      localStorage.setItem("reputation", JSON.stringify(data));
    }
  },
  actions: {
    setGameData({ commit }, data) {
      if (!data || !data.gameId) return commit("clearState");

      Object.entries(data).map(([key, value]) => {
        commit("setData", { key, value });
      });
    },
    updateGameData({ commit, state }, data) {
      Object.entries(data).map(([key, value]) => {
        const found = Object.prototype.hasOwnProperty.call(state, key);
        if (found) commit("setData", { key, value });
      });
    },
    setReputation({ commit, state }, data) {
      commit("setReputation", data);
      const value = getNumber(state.turn) + 1;
      commit("setData", { key: "turn", value });
    }
  },
  modules: {}
});
