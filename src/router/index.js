import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
import Welcome from "../views/Welcome";
import GameMain from "../views/GameMain";
import NotFound from "../views/NotFound";
import Store from "../views/Store";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Welcome",
    component: Welcome,
    meta: {
      requiresAuth: false,
      layout: "welcome"
    }
  },
  {
    path: "/game",
    name: "Game",
    component: GameMain,
    meta: {
      layout: "main",
      requiresAuth: true
    }
  },
  {
    path: "/shop",
    name: "Shop",
    component: Store,
    meta: {
      layout: "main",
      requiresAuth: true
    }
  },
  {
    path: "*",
    name: "404",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.gameId) {
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
