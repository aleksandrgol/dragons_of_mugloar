import axios from "axios";
import { API_URL } from "../constants";
import store from "../store";

export const getShop = async () => {
  const gameId = store.state.gameId;
  try {
    const shop = await axios.get(`${API_URL}/${gameId}/shop`);
    if (shop) {
      store.commit("setData", {
        key: "shop",
        value: shop.data
      });
      return shop.data;
    }
  } catch (error) {
    console.error("getShop:", error);
  }
};

export const purchaseItem = async itemId => {
  const gameId = store.state.gameId;
  try {
    const item = await axios.post(`${API_URL}/${gameId}/shop/buy/${itemId}`);
    if (item) {
      store.dispatch("updateGameData", item.data).then();
      return item.data;
    }
  } catch (error) {
    console.error("purchaseItem:", error);
  }
};
