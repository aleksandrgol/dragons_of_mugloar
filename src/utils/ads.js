import axios from "axios";
import { API_URL } from "../constants";
import store from "../store";

export const getAds = async () => {
  const gameId = store.state.gameId;
  try {
    const ads = await axios.get(`${API_URL}/${gameId}/messages`);
    if (ads) {
      // Filter encrypted ads (API fails fi encrypted)
      const data = ads.data.filter(ad => {
        if (!ad.encrypted) return ad;
      });
      store.commit("setData", {
        key: "ads",
        value: data
      });
    }
  } catch (error) {
    console.error("getAds:", error);
  }
};

export const solveAd = async adId => {
  const gameId = store.state.gameId;
  try {
    const result = await axios.post(`${API_URL}/${gameId}/solve/${adId}`);
    if (result) {
      store.dispatch("updateGameData", result.data).then();
      return result.data;
    }
  } catch (error) {
    console.error("solveAd:", error);
  }
};
