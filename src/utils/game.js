import axios from "axios";
import { API_URL } from "../constants";
import store from "../store";
import router from "../router";

export const startGame = async () => {
  try {
    const game = await axios.post(`${API_URL}/game/start`);
    if (game) {
      store.dispatch("setGameData", game.data).then(() => {
        router.push("/game");
      });
    }
  } catch (error) {
    console.error("startGame:", error);
  }
};

export const resumeGame = () => {
  router.push("/game");
};

export const endGame = () => {
  store.dispatch("setGameData", null).then(() => {
    router.push("/");
  });
};

export const getReputation = async () => {
  const gameId = store.state.gameId;
  try {
    const reputation = await axios.post(
      `${API_URL}/${gameId}/investigate/reputation`
    );
    if (reputation) {
      store.dispatch("setReputation", reputation.data).then();
      return reputation.data;
    }
  } catch (error) {
    console.error("getReputation:", error);
  }
};
