import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import "./assets/scss/grid.scss";
import "./assets/scss/styles.scss";

import MainLayout from "./layouts/MainLayout";
import WelcomeLayout from "./layouts/WelcomeLayout";
import Preloader from "./components/Preloader";

// Register layouts
Vue.component("main-layout", MainLayout);
Vue.component("welcome-layout", WelcomeLayout);
Vue.component("preloader", Preloader);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
