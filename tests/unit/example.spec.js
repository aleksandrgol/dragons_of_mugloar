import { shallowMount } from "@vue/test-utils";
import DummyTest from "@/components/DummyTest.vue";

describe("DummyTest.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(DummyTest, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });
});
